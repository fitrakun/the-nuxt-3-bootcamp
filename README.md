# The Nuxt 3 Bootcamp

This is a repository for my learning on the Nuxt 3 Bootcamp on Udemy. The course covers the complete development guide for Nuxt 3, a popular Vue.js framework for building fast and scalable web applications.

### Getting Started

To get started, follow these steps:

Clone this repository to your local machine using the following command:
`git clone https://gitlab.com/fitrakun/the-nuxt-3-bootcamp.git`

Navigate to the project directory:
`cd the-nuxt-3-bootcamp`

Install the dependencies:
`npm install`

Run the development server:
`npm run dev`

Open your browser and navigate to `http://localhost:3000` to view the application.

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.
