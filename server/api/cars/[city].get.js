import cars from "@/data/cars.json"
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()

export default defineEventHandler((event) => {
    const { city } = event.context.params;
    const { make, minPrice, maxPrice } = getQuery(event)

    // let filteredCars = cars
    
    // filteredCars = filteredCars.filter(car => car.city.toLowerCase() === city.toLowerCase());

    // if (make) {
    //     filteredCars = filteredCars.filter(car => car.make.toLowerCase() === make.toLowerCase());
    // }

    // if (minPrice) { 
    //     filteredCars = filteredCars.filter((car) =>
    //         car.price >= parseInt(minPrice));
    // }

    // if (maxPrice) {
    //     filteredCars = filteredCars.filter((car) =>
    //         car.price <= parseInt(maxPrice));
        
    // }

    const filters = {
        city: city.toLowerCase()
    }

    if (make) {
        filters.make = make
    }

    if (minPrice || maxPrice) {
        filters.price = {}
    }

    if (minPrice) {
        filters.price.gte = parseInt(minPrice)
    }

    if (maxPrice) {
        filters.price.lte = parseInt(maxPrice)
    }

    return prisma.car.findMany({
        where: filters
    });
})